//import DelayLoadingService from "@/components/LoadingService/DelayLoadingService";

export function isPromise(obj) {
  return (
    !!obj &&
    (typeof obj === "object" || typeof obj === "function") &&
    typeof obj.then === "function"
  );
}

//休眠方法
export const sleep = async (second) => {
  // eslint-disable-next-line no-unused-vars
  return new Promise((resolve, reject) => {
    window.setTimeout(() => {
      resolve();
    }, second);
  });
};

//根据json中配置的id找到对应的组件
export const findComponentByUUID = (uuid, root) => {
  let result = null;
  const visit = (uuid, node) => {
    for (let sub of node.$children) {
      if (sub?.$attrs?.uuid === uuid || sub?.uuid === uuid) {
        result = sub;
      }
      visit(uuid, sub);
    }
    return result;
  };
  visit(uuid, root);
  return result;
};


//根据uuid查找Schema的节点非map
export const findSchemaNodeByUUID = (uuid, root) => {
  let result = null;
  const visit = (uuid, node) => {
    for (let sub of node.items) {
      if (sub?.uuid === uuid) {
        result = sub;
      }
      visit(uuid, sub);
    }
    return result;
  };
  visit(uuid, root);
  return result;
};



// // eslint-disable-next-line no-unused-vars
// export function loading(opts = { delay: 0 }) {
//   // eslint-disable-next-line no-unused-vars
//   return function(target, propertyKey, descriptor) {
//     let originFn = descriptor.value;
//     descriptor.value = function(...p) {
//       //console.log(`Calling ${propertyKey} with`, p);
//       DelayLoadingService.getService().openLoading();
//       try {
//         let fnResult = originFn.apply(this, p);
//         if (isPromise(fnResult)) {
//           return fnResult
//             .then((ret) => {
//               if (opts.delay === 0) {
//                 DelayLoadingService.getService().closeLoadingImmediate();
//               } else {
//                 DelayLoadingService.getService().closeLoading(opts.delay);
//               }
//               return ret;
//             })
//             .catch((err) => {
//               DelayLoadingService.getService().closeLoadingImmediate();
//               //此处抛给promise链而不是本函数自身外侧的catch方法,并且本函数外部的catch在关闭loading后也做了抛出,
//               //所以异常未被吃掉,特别注意
//               return Promise.reject(err); //或 throw err;
//             });
//         } else {
//           DelayLoadingService.getService().closeLoadingImmediate();
//           return fnResult;
//         }
//       } catch (err) {
//         DelayLoadingService.getService().closeLoadingImmediate();
//         throw err;
//       }
//     };
//     return descriptor;
//   };
// }

import Vue from "vue";
import _ from "lodash";
import { v4 as uuidv4 } from 'uuid';

Vue.filter("_nil", (value, ret = "") => {
  return value ?? ret;
});

Vue.filter("_path", (value, path, ret = "") => {
  return _.get(value, path, ret);
});

Vue.filter("_frm_data", (value) => {
  return value !== null && value !== undefined ? { formData: value } : {};
});


// eslint-disable-next-line no-unused-vars
Vue.filter("_uuid", (value) => {
  return uuidv4();
});

import Vue from "vue";
import VueRouter from "vue-router";
//import Home from '../views/Home.vue'
//import QDesign from '../views/QDesign.vue'

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    exact: true,
    redirect: "/design",
    //component: Home
  },
  {
    path: "/design",
    name: "QDesign",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/QDesign.vue"),
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
];

const router = new VueRouter({
  routes,
});

export default router;

import Vue from "vue";
import Vuex from "vuex";
import store from './store'

import router from "./router";
import "@/utils/filters";

import ElementUI from "element-ui";
import Fragment from "vue-fragment";

//低代码平台组件引入
import QEntry from "@/components/QEntry";

import "normalize.css/normalize.css";
import "element-ui/lib/theme-chalk/index.css";
import draggable from "vuedraggable";

import VueCodemirror from "vue-codemirror";
import "codemirror/lib/codemirror.css";

// import language js
import "codemirror/mode/javascript/javascript.js";
// import theme style
import "codemirror/theme/base16-dark.css";

import App from "./App.vue";

// you can set default global options and events when Vue.use
Vue.use(VueCodemirror, {
  options: {
    tabSize: 4,
    styleActiveLine: true,
    lineNumbers: true,
    line: true,
    mode: "text/javascript",
    lineWrapping: true,
    theme: "base16-dark",
    readOnly: true,
  },
  // events: ["scroll"],
});

//低代码平台组件引入
Vue.use(QEntry);

Vue.use(Vuex);
Vue.use(ElementUI);
Vue.use(Fragment.Plugin);
Vue.component("draggable", draggable);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");

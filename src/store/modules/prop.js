const state = {
  editing: "",
  map: new Map(),
};

const mutations = {
  SET_EDITING(state, { uuid }) {
    state.editing = uuid;
  },
  SET_MAP(state, { map }) {
    state.map = map;
  },
};

const actions = {
  editProps(context, { uuid }) {
    context.commit("SET_EDITING", { uuid: uuid });
  },
  setMap(context, { map }) {
    context.commit("SET_MAP", { map: map });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};

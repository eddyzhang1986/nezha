import Vue from "vue";
import Vuex from "vuex";
import getters from "./getters";
import prop from "./modules/prop";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    prop,
  },
  getters,
});

export default store;

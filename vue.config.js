module.exports = {
  devServer: {
    port: 8082,
    open: true,
    overlay: {
      warnings: false,
      errors: true,
    },
    proxy: {
      "/api": {
        target: "https://www.parseserver.com/", // 测试
        changeOrigin: true,
      },
    },
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "@/assets/style/variable.scss" ;`,
      },
    },
  },
};
